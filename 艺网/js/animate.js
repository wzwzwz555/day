function animate(element, target, propName, speed, fn) {
    //propName :属性的名称
    // 通过判断，保证页面上只有一个定时器在执行动画
    if (element.timerId) {
        clearInterval(element.timerId);
        element.timerId = null;
    }

    element.timerId = setInterval(function() {
        // 步进  每次移动的距离
        var step = 10;
        // 获取当前的值
        // var current = element.offsetLeft;

        var current = parseInt(window.getComputedStyle(element, null)[propName]) || 0; // "100px"

        // 判断如果当前位置 > 目标位置 此时的step  要小于0
        if (current > target) {
            step = -Math.abs(step);
        }

        if (Math.abs(current - target) <= Math.abs(step)) {
            // 让定时器停止
            clearInterval(element.timerId);
            // 让flag设置为true
            if (fn) {
                fn();
            }
            // 让盒子到target的位置
            element.style[propName] = target + 'px';
            return;
        }

        // 移动盒子
        current += step;
        element.style[propName] = current + 'px';
    }, speed);
}