$(function(){
    var index = 0;
    $(".North_2_rn ul").append($(".North_2_1_rn").clone());
    $(".North_2_rn ul").append($(".North_2_2_rn").clone());
    $(".North_2_rn ul").append($(".North_2_3_rn").clone());

    $(".North_2_arrRight_rn").click(function(){
        index++;
        if(index <= 5){
            $(".North_2_rn>ul").animate({"marginLeft":-590*index},500)
        }else{
            index = 0;
            $(".North_2_rn>ul").css("marginLeft","0px");

            index++;
            $(".North_2_rn>ul").animate({"marginLeft":-590*index},500)
        }

    })

    $(".North_2_arrLeft_rn").click(function(){
         if(index <= 0){
             index = 5;
            $(".North_2_rn>ul").css("marginLeft", -590*index);
            index--;
            $(".North_2_rn>ul").animate({"marginLeft":-590*index},500)
        }else{
            index--;
            $(".North_2_rn>ul").animate({"marginLeft":-590*index},500)
        }
    })

    $(".North_4_1_rn>ul>li").on("click","img",function(){
        $(this).css("border","1px solid red").parent("li").siblings("li").find("img").css("border","");
        var path = $(this).attr("src");
        console.log(path);
        $(".North_2_hide_rn").attr("src",path);
    })

    
    $(".painting_rn>div>ul>li").mouseenter(function(){
        $(this).find($(".painting_rn_2_4")).stop().fadeIn(500).css("marginTop","20");
    })
    $(".painting_rn>div>ul>li").mouseleave(function(){
        $(this).find($(".painting_rn_2_4")).stop().fadeOut(500).css("marginTop","");
    })
})